#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/generator'

errors = []
Dir.glob('**/*.yaml').each do |file|
  print "Checking #{file}: "
  begin
    YAML.safe_load(File.read(file))
    puts 'Readable'
  rescue Psych::SyntaxError
    errors.push("#{file} cannot be read")
    puts 'ERROR - Unable to be read!'
  end
end
Dir.glob('data/announcements/*.yaml').each do |file|
  begin
    yaml = YAML.safe_load(File.read(file))
  rescue Psych::SyntaxError
    next
  end
  if yaml.nil?
    errors.push("#{file} contains no YAML data")
  elsif yaml['announcements'].nil?
    errors.push("#{file} is missing the announcements array")
  end
end
Dir.glob('data/comments/*.yaml').each do |file|
  begin
    yaml = YAML.safe_load(File.read(file))
  rescue Psych::SyntaxError
    next
  end
  if yaml.nil?
    errors.push("#{file} contains no YAML data")
  else
    errors.push("#{file} is missing the ticket_review boolean") if yaml['ticket_review'].nil?
    errors.push("#{file} is missing the comments array") if yaml['comments'].nil?
  end
end
Dir.glob('data/templates/*erb').each do |file|
  print "Checking #{file}: "
  erb = ERB.new(File.read(file))
  begin
    erb.result(binding)
    puts 'Readable'
  rescue SyntaxError
    errors.push("#{file} is not readable")
    puts 'Unreadable'
  rescue NameError
    puts 'Readable'
  end
end
unless errors.count.zero?
  msg = "\n\nTests failed for the following reasons:\n"
  errors.each do |e|
    msg << "* #{e}\n"
  end
  abort(msg)
end
