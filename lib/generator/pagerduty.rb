# frozen_string_literal: true

# Generator module
module Generator
  # Pagerduty class
  class Pagerduty < Client
    def self.agents(object = nil)
      @agents ||= object
    end

    def self.current_start_date
      if Date.today.wday == 1
        Date.today
      else
        Date.today.prev_occurring(:monday)
      end
    end

    def self.current_end_date
      Date.today.next_occurring(:sunday)
    end

    def self.next_start_date
      if Date.today.wday.zero?
        Date.today.next_occurring(:monday) + 7
      else
        Date.today.next_occurring(:monday)
      end
    end

    def self.next_end_date
      Date.today.next_occurring(:sunday) + 7
    end

    def self.current_options(tzn)
      "time_zone=#{tzn}&since=#{current_start_date}&until=#{current_end_date}"
    end

    def self.next_options(tzn)
      "time_zone=#{tzn}&since=#{next_start_date}&until=#{next_end_date}"
    end

    def self.schedules
      @schedules ||= gather_schedules
    end

    def self.gather_schedules
      url = 'https://gitlab.com/api/v4/projects/19195161/repository/files/data%2Fstatic_data.yaml/raw?ref=master'
      response = gl_client.public_send(:get, url)
      YAML.safe_load(response.body, [Date])['oncall']
    end

    def self.determine_scheduled
      print 'Gathering PD scedules'
      schedules.each do |info|
        print '.'
        determine_current(info)
        determine_next(info)
      end
      puts 'done'
      @agents
    end

    def self.determine_current(info)
      res = pd_request(:get, "/schedules/#{info['id']}?#{current_options(info['time_zone'])}")
      check_schedules(info['name'], res['schedule']['final_schedule']['rendered_schedule_entries'], true)
    end

    def self.determine_next(info)
      res = pd_request(:get, "/schedules/#{info['id']}?#{next_options(info['time_zone'])}")
      check_schedules(info['name'], res['schedule']['final_schedule']['rendered_schedule_entries'], false)
    end

    def self.check_schedules(name, schedules, current)
      schedules.each do |s|
        check_user(name, s, current)
      end
    end

    def self.check_user(name, schedule, current)
      user = @agents.detect { |u| schedule['user']['summary'] == u['name'] }
      return if user.nil?
      return if user['pagerduty'].include? name.to_s

      if current
        user['pagerduty']['data'][:this_week].push(name.to_s)
      else
        user['pagerduty']['data'][:next_week].push(name.to_s)
      end
    end

    def self.gather!(object)
      agents(object)
      determine_scheduled
    end
  end
end
