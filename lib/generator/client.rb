# frozen_string_literal: true

# Generator module
module Generator
  # Client class
  class Client
    require_relative 'gitlab'
    require_relative 'pagerduty'
    # require_relative 'zendesk'

    def self.run!
      data
      artifact
    end

    def self.artifact
      date = Date.today.to_time.in_time_zone('UTC') - 1.days
      open("data/#{date.strftime('%Y-%m-%d')}_analysis.json", 'w') { |f| f.puts data.to_json }
    end

    def self.data
      @data ||= gather_all_data
    end

    def self.gather_all_data
      agents
      # Generator::Zendesk::Global.gather!(@agents)
      # Generator::Zendesk::USFederal.gather!(@agents)
      Generator::GitLab::Gather.gather!(@agents)
      Generator::Pagerduty.gather!(@agents)
      @agents
    end

    def self.retry_options
      {
        max: 5,
        interval: 1,
        interval_randomness: 0.5,
        backoff_factor: 2,
        exceptions: [Faraday::ConnectionFailed, Faraday::TimeoutError]
      }
    end

    def self.gl_admin_client
      @gl_admin_client ||= Faraday.new('https://gitlab.com/api/v4') do |c|
        c.request :retry, retry_options
        c.request :url_encoded
        c.adapter Faraday.default_adapter
        c.headers['Private-Token'] = ENV.fetch('GL_ADMIN_TOKEN')
      end
    end

    def self.gl_admin_request(method, url, params = {})
      response = gl_admin_client.public_send(method, url, params)
      Oj.load(response.body)
    end

    def self.gl_client
      @gl_client ||= Faraday.new('https://gitlab.com/api/v4') do |c|
        c.request :retry, retry_options
        c.request :url_encoded
        c.adapter Faraday.default_adapter
        c.headers['Private-Token'] = ENV.fetch('GL_TOKEN')
      end
    end

    def self.gl_request(method, url, params = {})
      response = gl_client.public_send(method, url, params)
      Oj.load(response.body)
    end

    def self.zd_global_client
      @zd_global_client ||= Faraday.new(ENV.fetch('ZD_URL')) do |config|
        config.request :retry, retry_options
        config.adapter Faraday.default_adapter
        config.headers['Content-Type'] = 'application/json'
        config.request :basic_auth, "#{ENV.fetch('ZD_USERNAME')}/token", ENV.fetch('ZD_TOKEN')
      end
    end

    def self.zd_global_request(http_method, url, params = {})
      response = zd_global_client.public_send(http_method, url, params)
      Oj.load(response.body)
    end

    def self.zd_usfederal_client
      @zd_usfederal_client ||= Faraday.new(ENV.fetch('US_ZD_URL')) do |config|
        config.request :retry, retry_options
        config.adapter Faraday.default_adapter
        config.headers['Content-Type'] = 'application/json'
        config.request :basic_auth, "#{ENV.fetch('US_ZD_USERNAME')}/token", ENV.fetch('US_ZD_TOKEN')
      end
    end

    def self.zd_usfederal_request(http_method, url, params = {})
      response = zd_usfederal_client.public_send(http_method, url, params)
      Oj.load(response.body)
    end

    def self.pd_client
      @pd_client ||= Faraday.new('https://api.pagerduty.com') do |c|
        c.request :retry, retry_options
        c.headers['Authorization'] = "Token token=#{ENV.fetch('PD_TOKEN')}"
        c.headers['Accept'] = 'application/vnd.pagerduty+json;version=2'
        c.adapter :net_http
      end
    end

    def self.pd_request(http_method, endpoint, params = {})
      response = pd_client.public_send(http_method, endpoint, params)
      Oj.load(response.body)
    end

    def self.support_team_yaml
      url = 'https://gitlab.com/api/v4/projects/19195161/repository/files/data%2Fsupport-team.yaml/raw?ref=master'
      response = gl_client.public_send(:get, url)
      YAML.safe_load(response.body, [Date])
    end

    def self.agents
      @agents ||= determine_agents
    end

    def self.determine_agents
      users = []
      support_team_yaml.each do |agent|
        next if agent['1-1-generator'].nil?
        next if agent['1-1-generator']['project_id'].to_i.zero?

        user = setup_user(agent)
        users.push(user)
      end
      users
    end

    def self.setup_user(agent)
      agent['zendesk']['main']['data'] = zd_object
      agent['zendesk']['us-federal']['data'] = zd_object
      agent['gitlab']['data'] = gitlab_data_object
      agent['pagerduty']['data'] = { this_week: [], next_week: [] }
      agent
    end

    def self.zd_object
      {
        assigned: [],
        public_comments: [],
        private_comments: [],
        ssat: [],
        ccs: []
      }
    end

    def self.gitlab_data_object
      {
        pairings: { current: [], previous: [] },
        internal: { current: [], previous: [] },
        interviews: { current: [], previous: [] },
        modules: { current: [], previous: [] },
        handbook: { current: [], previous: [] },
        documentation: { current: [], previous: [] },
        frontend: { current: [], previous: [] },
        backend: { current: [], previous: [] },
        design: { current: [], previous: [] },
        support_fix: { current: [], previous: [] },
        other: { current: [], previous: [] },
        open_mrs: [],
        open_issues: []
      }
    end
  end
end
