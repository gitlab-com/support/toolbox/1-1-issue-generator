# frozen_string_literal: true

# Generator module
module Generator
  # Zendesk class
  class Zendesk
    # Global class
    class Global < Client
      def self.gather!(object)
        agents(object)
        print 'Gathering ZD Global tickets'
        gather_tickets
        puts 'done'
        agents
      end

      def self.agents(object = nil)
        @agents ||= object
      end

      def self.agent(zd_id)
        agents.detect { |a| a['zendesk']['main']['id'] == zd_id } || nil
      end

      def self.start_time
        (Date.today.to_time.in_time_zone('UTC') - 1.days).beginning_of_day
      end

      def self.end_time
        Date.today.to_time.in_time_zone('UTC').beginning_of_day
      end

      def self.gather_tickets
        more = true
        page = 11
        while more
          print '.'
          opts = 'include=organizations&sort_by=updated_at&sort_order=desc'
          response = zd_global_request(:get, "tickets?#{opts}&page=#{page}")
          page += 1
          next if response['tickets'].last['updated_at'] > end_time

          analyze_tickets(response)
          more = false if response['tickets'].last['updated_at'] < start_time
          page += 1
        end
      end

      def self.analyze_tickets(response)
        response['tickets'].each do |ticket|
          next if ticket['updated_at'] > end_time
          break if ticket['updated_at'] <= start_time

          ticket['organization'] = organization(ticket, response['organizations'])
          check_assignee(ticket)
          check_ccs(ticket)
          check_ssat(ticket)
          check_comments(ticket)
        end
      end

      def self.check_assignee(ticket)
        return if ticket['assignee_id'].nil?

        assignee = agent(ticket['assignee_id'])
        return if assignee.nil?

        assignee['zendesk']['main']['data'][:assigned].push(ticket_object(ticket, nil))
      end

      def self.check_ccs(ticket)
        return if ticket['collaborator_ids'].count.zero?

        ticket['collaborator_ids'].each do |id|
          cc = agent(id)
          next if cc.nil?

          cc['zendesk']['main']['data'][:ccs].push(ticket_object(ticket, nil))
        end
      end

      def self.check_ssat(ticket)
        return if ticket['satisfaction_rating']['score'] == 'unoffered'
        return if ticket['satisfaction_rating']['score'] == 'offered'
        return if ticket['assignee_id'].nil?

        assignee = agent(ticket['assignee_id'])
        return if assignee.nil?

        assignee['zendesk']['main']['data'][:ssat].push(ticket_object(ticket, nil))
      end

      def self.check_comments(ticket)
        response = zd_global_request(:get, "tickets/#{ticket['id']}/comments?sort=created_at&sort_order=desc")
        response['comments'].each do |comment|
          next if comment['created_at'] >= end_time
          break if comment['created_at'] < start_time

          analyze_comment(ticket, comment)
        end
      end

      def self.analyze_comment(ticket, comment)
        commenter = agent(comment['author_id'])
        return if commenter.nil?

        object = ticket_object(ticket, comment['id'])
        if comment['public']
          commenter['zendesk']['main']['data'][:public_comments].push(object)
        else
          commenter['zendesk']['main']['data'][:private_comments].push(object)
        end
      end

      def self.organization(ticket, orgs)
        return '' if ticket['organization_id'].nil?

        orgs.detect { |o| o['id'] == ticket['organization_id'] }['name']
      rescue NoMethodError
        ''
      end

      def self.ticket_object(ticket, comment)
        {
          id: ticket['id'],
          subject: ticket['subject'],
          status: ticket['status'],
          assignee: ticket['assignee_id'],
          ssat: ticket['satisfaction_rating']['score'],
          org: ticket['organization'],
          updated_at: ticket['updated_at'],
          comment_id: comment
        }
      end
    end
  end
end
