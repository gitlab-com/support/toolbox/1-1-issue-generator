# frozen_string_literal: true

# Generator module
module Generator
  # Zendesk class
  class Zendesk
    require_relative 'zendesk/global'
    require_relative 'zendesk/us_federal'
  end
end
