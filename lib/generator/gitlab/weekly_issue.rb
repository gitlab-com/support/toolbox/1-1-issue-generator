# frozen_string_literal: true

# Generator module
module Generator
  # GitLab module
  module GitLab
    # WeeklyIssue class
    class WeeklyIssue < Client
      def self.data
        @data ||= analyze_reports
      end

      def self.title(agent)
        date = Date.today.to_time.in_time_zone('UTC').strftime('%Y-%m-%d')
        "#{agent['name']} 1-1: #{date}"
      end

      def self.week_of_reports
        @week_of_reports ||= fetch_reports
      end

      def self.fetch_reports
        reports = []
        puts 'Gathering reports'
        today = Date.today - 1.days
        (today.weeks_ago(1)..today).map { |d| d.strftime('%Y-%m-%d') }.each do |d|
          puts "  Getting report for #{d}"
          report = daily_report(d)
          next if report.count == 1

          reports.push(report)
        end
        reports
      end

      def self.daily_report(date)
        url = "https://gitlab.com/api/v4/projects/26161478/repository/files/reports%2F#{date}_analysis.json/raw?ref=master"
        response = gl_client.public_send(:get, url)
        JSON.parse(response.body)
      end

      def self.analyze_reports
        # week_of_reports.last.each do |agent|
          # agent['zendesk'] = combine_reports(agent)
        # end
        week_of_reports.last
      end

      def self.combine_reports(agent)
        zd = clear_zd_object(agent)
        week_of_reports.to_a.each do |report|
          user = report.detect { |r| r['name'] == agent['name'] }
          next if user.nil?

          %w[main us-federal].each do |instance|
            %w[assigned public_comments private_comments ssat ccs].each do |type|
              zd[instance]['data'][type].concat(gather_tickets(user, instance, type))
            end
          end
        end
        zd
      end

      def self.gather_tickets(user, instance, type)
        start_time = (Date.today - 7.days).beginning_of_day
        end_time = (Date.today - 1.days).end_of_day
        tickets = []
        user['zendesk'][instance]['data'][type].each do |t|
          next unless t['updated_at'] >= start_time
          next unless t['updated_at'] <= end_time

          tickets.push(t)
        end
        tickets
      end

      def self.clear_zd_object(agent)
        zd = agent['zendesk']
        %w[main us-federal].each do |instance|
          %w[assigned public_comments private_comments ssat ccs].each do |type|
            zd[instance]['data'][type] = []
          end
        end
        zd
      end

      def self.user_template(user)
        if File.exist? "data/templates/#{user['email'].split('@').first}.erb"
          return "data/templates/#{user['email'].split('@').first}.erb"
        end

        'data/templates/weekly_report.erb'
      end

      def self.run_today?(agent)
        wday = Date.today.to_time.in_time_zone('UTC').wday
        return true if agent['1-1-generator']['day_of_week'] == wday

        false
      end

      def self.create
        data.each do |agent|
          next if agent['1-1-generator'].empty?
          next if agent['1-1-generator']['project_id'].to_s.empty?
          next unless run_today?(agent)

          create_agent_issue(agent)
        end
      end

      def self.create_agent_issue(agent)
        puts "Creating issue for #{agent['name']}"
        erb_renderer = ERB.new(File.read(user_template(agent)))
        issue_opts = {
          title: title(agent),
          description: erb_renderer.result_with_hash(user: agent, users: data),
          confidential: true,
          labels: agent['1-1-generator']['labels'],
          assignee_ids: [agent['gitlab']['id']]
        }
        iid = create_issue(agent, issue_opts)
        puts 'Sleeping for 15 seconds...'
        sleep 15
        comment_on_issue(agent, iid)
      end

      def self.comment_on_issue(agent, iid)
        comments = []
        comments.concat(comment_file(agent))
        comments.concat(announcement_file(agent))
        comments.concat(agenda_file(agent))
        comments.each do |c|
          create_comment(c, agent, iid)
        end
      end

      def self.comment_file(agent)
        # tickets = ticket_review(agent)
        comments = from_comment_file(agent)
        #comments.concat(tickets)
      end

      def self.slug(agent)
        agent['email'].split('@').first
      end

      def self.ticket_review(agent)
        if File.exist? "data/comments/#{slug(agent)}.yaml"
          begin
            file = YAML.safe_load(File.read("data/comments/#{slug(agent)}.yaml"))
            return [] unless file['ticket_review']
          rescue Psych::SyntaxError
            puts "***Bad comment file for #{agent['name']}, using default***"
          end
        end
        random_tickets(agent)
      end

      def self.random_tickets(agent)
        main = agent['zendesk']['main']['data']['public_comments'].uniq.map do |t|
          "## Ticket Review: [#{t['subject']}](https://gitlab.zendesk.com/agent/tickets/#{t['id']})"
        end
        usfed = agent['zendesk']['us-federal']['data']['public_comments'].uniq.map do |t|
          "## Ticket Review: [#{t['subject']}](https://gitlab-federal-support.zendesk.com/agent/tickets/#{t['id']})"
        end
        main.concat(usfed).sample(3)
      end

      def self.from_comment_file(agent)
        if File.exist? "data/comments/#{slug(agent)}.yaml"
          begin
            file = YAML.safe_load(File.read("data/comments/#{slug(agent)}.yaml"))
            return file['comments']
          rescue Psych::SyntaxError
            puts "***Bad comment file for #{agent['name']}, using default***"
          end
        end
        file = YAML.safe_load(File.read('data/comments/default.yaml'))
        file['comments']
      end

      def self.announcement_file(agent)
        manager = data.detect { |d| d['name'] == agent['reports_to'] }
        return [] if manager.nil?

        if File.exist? "data/announcements/#{slug(manager)}.yaml"
          begin
            file = YAML.safe_load(File.read("data/announcements/#{slug(manager)}.yaml"))
            return file['announcements'].map do |a|
              "## #{a['title']}\n\n#{a['value']}"
            end
          rescue Psych::SyntaxError
            puts "***Bad announcements file for #{manager['name']}, skipping***"
          end
        end
        []
      end

      def self.agenda_file(agent)
        comment = gather_agenda_file(agent)
        return [] unless comment
        return [] if comment.first.empty?

        empty_file(agent)
        comment
      end

      def self.gather_agenda_file(agent)
        id = agent['1-1-generator']['project_id']
        url = "https://gitlab.com/api/v4/projects/#{id}/repository/files/agenda.md/raw?ref=master"
        response = gl_client.public_send(:get, url)
        pp response.body
        return false if response.body.empty?

        begin
          file = JSON.parse(response.body)
          return false if file['message']

          [file['content']]
        rescue JSON::ParserError
          [response.body]
        end
      end

      def self.empty_file(agent)
        id = agent['1-1-generator']['project_id']
        path = "#{id}/repository/files/agenda.md/raw?ref=master"
        gl_request(:post, path, empty_file_opts)
      end

      def self.empty_file_opts
        {
          branch: 'master',
          commit_message: 'Reset Agenda',
          actions: [
            {
              action: 'update',
              file_path: 'agenda.md',
              content: ''
            }
          ]
        }
      end

      def self.create_issue(agent, opts)
        path = "projects/#{agent['1-1-generator']['project_id']}/issues"
        res = gl_request(:post, path, opts)
        puts res
        res['iid']
      end

      def self.create_comment(comment, agent, iid)
        puts 'DEBUG INFO FOR COMMENTS'
        puts "* Agent Name: #{agent['name']}"
        puts "* IID: #{iid}"
        puts "* Comment: #{comment}"
        path = "projects/#{agent['1-1-generator']['project_id']}/issues/#{iid}/notes"
        req = gl_request(:post, path, { body: comment })
        puts '* Request Response:'
        pp req
      end
    end
  end
end
