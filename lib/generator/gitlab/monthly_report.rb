# frozen_string_literal: true

# Generator module
module Generator
  # GitLab module
  module GitLab
    # MonthlyReport class
    class MonthlyReport < Client
      def self.data
        @data ||= analyze_reports
      end

      def self.run
        check_if_valid_agent
        data
        generate_issue
      end

      def self.analyze_reports
        agent = determine_agent(month_of_reports.last)
        # agent['zendesk'] = combine_zendesk_reports(agent)
        agent['gitlab'] = combine_gitlab_reports(agent)
        agent
      end

      def self.determine_agent(report, name = ENV.fetch('AGENT_NAME'))
        report.detect { |u| u['name'] == name }
      end

      def self.month_of_reports
        @month_of_reports ||= fetch_reports
      end

      def self.daily_report(date)
        url = "https://gitlab.com/api/v4/projects/26161478/repository/files/reports%2F#{date}_analysis.json/raw?ref=master"
        response = gl_client.public_send(:get, url)
        JSON.parse(response.body)
      end

      def self.fetch_reports
        reports = []
        today = Date.today - 1.days
        (today.months_ago(1)..today).map { |d| d.strftime('%Y-%m-%d') }.each do |d|
          puts "  Getting report for #{d}"
          report = daily_report(d)
          next if report.count == 1

          reports.push(report)
        end
        reports
      end

      def self.title(agent)
        date = Date.today.to_time.in_time_zone('UTC').strftime('%Y-%m-%d')
        "#{agent['name']} Monthly Report: #{date}"
      end

      def self.check_if_valid_agent
        user = agents.detect { |u| u['name'] == ENV['AGENT_NAME'] }
        raise('No such agent') if user.nil?

        true
      end

      def self.combine_zendesk_reports(agent)
        zd = clear_zd_object(agent)
        month_of_reports.to_a.each_with_index do |report, index|
          break if (index + 1) == month_of_reports.count
          next if report.count == 1

          user = report.detect { |r| r['name'] == agent['name'] }
          next if user.nil?

          %w[main us-federal].each do |instance|
            %w[assigned public_comments private_comments ssat ccs].each do |type|
              zd[instance]['data'][type].concat(gather_tickets(user, instance, type))
            end
          end
        end
        zd
      end

      def self.gather_tickets(user, instance, type)
        start_time = (Date.today - 7.days).beginning_of_day
        end_time = (Date.today - 1.days).end_of_day
        tickets = []
        user['zendesk'][instance]['data'][type].each do |t|
          next unless t['updated_at'] >= start_time
          next unless t['updated_at'] <= end_time

          tickets.push(t)
        end
        tickets
      end

      def self.clear_zd_object(agent)
        zd = agent['zendesk']
        %w[main us-federal].each do |instance|
          %w[assigned public_comments private_comments ssat ccs].each do |type|
            zd[instance]['data'][type] = []
          end
        end
        zd
      end

      def self.combine_gitlab_reports(agent)
        gitlab = agent['gitlab']
        month_of_reports.each_with_index do |report, index|
          break if (index + 1) == month_of_reports.count
          next if report.count == 1

          user = report.detect { |r| r['name'] == agent['name'] }
          %w[pairings interviews modules handbook documentation frontend backend
             design support_fix other].each do |type|
            %w[current previous].each do |time|
              gitlab['data'][type][time].concat(user['gitlab']['data'][type][time])
            end
          end
        end
        squash_gitlab_section(gitlab)
      end

      def self.squash_gitlab_section(gitlab)
        %w[pairings interviews modules handbook documentation frontend backend
           design support_fix other].each do |type|
          gitlab['data'][type] = gitlab['data'][type]['current'].concat(gitlab['data'][type]['previous'])
        end
        gitlab
      end

      def self.generate_issue
        puts "Creating issue for #{data['name']}"
        erb_renderer = ERB.new(File.read('data/templates/monthly_report.erb'))
        issue_opts = {
          title: title(data),
          description: erb_renderer.result_with_hash(user: data)
        }
        create_issue(data, issue_opts)
      end

      def self.create_issue(agent, opts)
        path = "projects/#{agent['1-1-generator']['project_id']}/issues"
        res = gl_request(:post, path, opts)
        res['iid']
      end
    end
  end
end
