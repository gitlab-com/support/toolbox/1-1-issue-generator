# frozen_string_literal: true

# Generator module
module Generator
  # GitLab module
  module GitLab
    # Gather class
    class Gather < Client
      def self.gather!(object)
        agents(object)
        puts 'Gathering GL data'
        pairings
        internal_requests
        interviews
        modules
        handbook
        open_issues
        open_mrs
        puts 'done'
        agents
      end

      def self.agents(object = nil)
        @agents ||= object
      end

      def self.agent(gl_id)
        agents.detect { |a| a['gitlab']['id'] == gl_id } || nil
      end

      def self.this_week_start
        (Date.today.wday.zero? ? Date.today : Date.today.prev_occurring(:sunday))
      end

      def self.this_week_end
        Date.today.next_occurring(:sunday)
      end

      def self.last_week_start
        (Date.today.wday.zero? ? Date.today.prev_occurring(:sunday) : (Date.today.prev_occurring(:sunday) - 7))
      end

      def self.last_week_end
        (Date.today.wday.zero? ? Date.today : Date.today.prev_occurring(:sunday))
      end

      def self.last_month_start
        Date.today.months_ago(1).beginning_of_month
      end

      def self.last_month_end
        Date.today.beginning_of_month - 1.day
      end

      def self.this_month_start
        Date.today.beginning_of_month
      end

      def self.this_month_end
        Date.today.end_of_month + 1.day
      end

      def self.last_quarter_start
        case Date.today.month
        when 1     then Date.parse('August').last_year
        when 2..4  then Date.parse('November').last_year
        when 5..7  then Date.parse('February')
        when 8..10 then Date.parse('May')
        else            Date.parse('August')
        end
      end

      def self.last_quarter_end
        case Date.today.month
        when 1     then Date.parse('October 31').last_year
        when 2..4  then Date.parse('January 31')
        when 5..7  then Date.parse('April 30')
        when 8..10 then Date.parse('July 31')
        else            Date.parse('October 31')
        end
      end

      def self.this_quarter_start
        case Date.today.month
        when 1     then Date.parse('November').last_year
        when 2..4  then Date.parse('February')
        when 5..7  then Date.parse('May')
        when 8..10 then Date.parse('August')
        else            Date.parse('November')
        end
      end

      def self.this_quarter_end
        case Date.today.month
        when 1      then Date.parse('January 31')
        when 2..4   then Date.parse('April 30')
        when 5..7   then Date.parse('July 31')
        when 8..10  then Date.parse('October 31')
        else             Date.parse('January 31').next_year
        end
      end

      def self.pairings
        print '  Pairings'
        %w[pairing-global pairing-us-federal].each do |label|
          gather_issues_by_creation(last_week_start, this_week_start, this_week_end, :pairings, label, 14978605)
        end
        puts 'done'
      end

      def self.internal_requests
        print '  Internal Requests'
        label = 'support-internal-request'
        gather_issues_by_creation(last_week_start, this_week_start, this_week_end, :internal, label, 7083173)
        puts 'done'
      end

      def self.interviews
        print '  Interviews'
        gather_issues_by_closed(last_week_start, this_week_start, this_week_end, :interviews, nil, 1734706)
        puts 'done'
      end

      def self.modules
        print '  Modules'
        gather_issues_by_closed(last_quarter_start, this_quarter_start, this_quarter_end, :modules, nil, 6282913)
        puts 'done'
      end

      def self.handbook
        print '  Handbook MRs'
        gather_handbook_mrs
        puts 'done'
      end

      def self.open_issues
        agents.each do |agent|
          print "  Gathering open issues for #{agent['name']}"
          gather_open_issues(agent)
          puts 'done'
        end
      end

      def self.open_mrs
        agents.each do |agent|
          print "  Gathering open MRs for #{agent['name']}"
          gather_open_mrs(agent)
          puts 'done'
        end
      end

      def self.gather_issues_by_creation(start_date, mid_date, end_date, type, label = nil, project_id)
        more = true
        page = 1
        while more
          print '.'
          opts = if label
                   "per_page=100&labels=#{label}&created_after=#{start_date.strftime('%Y-%m-%dT00:00:00Z')}"
                 else
                   "per_page=100&created_after=#{start_date.strftime('%Y-%m-%dT00:00:00Z')}"
                 end
          response = gl_request(:get, "projects/#{project_id}/issues?#{opts}&page=#{page}")
          response.each do |issue|
            next if Date.parse(issue['created_at']) > end_date

            analyze_issue_by_creation(type, mid_date, issue)
          end
          page += 1
          more = false if response.count < 100
        end
      end

      def self.gather_issues_by_closed(start_date, mid_date, end_date, type, label = nil, project_id)
        more = true
        page = 1
        while more
          print '.'
          opts = if label
                   "per_page=100&labels=#{label}&updated_after=#{start_date.strftime('%Y-%m-%dT00:00:00Z')}"
                 else
                   "per_page=100&created_after=#{start_date.strftime('%Y-%m-%dT00:00:00Z')}"
                 end
          response = gl_request(:get, "projects/#{project_id}/issues?#{opts}&page=#{page}")
          response.each do |issue|
            next if issue['closed_at'].nil?
            next if Date.parse(issue['closed_at']) > end_date

            analyze_issue_by_closed(type, mid_date, issue)
          end
          page += 1
          more = false if response.count < 100
        end
      end

      def self.gather_handbook_mrs
        # https://gitlab.com/gitlab-com/www-gitlab-com
        more = true
        page = 1
        while more
          print '.'
          start_time = last_month_start.strftime('%Y-%m-%dT00:00:00Z')
          opts = "scope=all&state=merged&per_page=100&updated_after=#{start_time}&order_by=updated_at&sort=desc"
          response = gl_request(:get, "projects/7764/merge_requests?#{opts}&page=#{page}")
          response.each do |merge_request|
            next if merge_request['merged_at'].nil?
            next if Date.parse(merge_request['merged_at']) > this_month_end

            analyze_handbook_mr(merge_request)
          end
          page += 1
          more = false if response.count < 100
        end
      end

      def self.gather_open_issues(agent)
        more = true
        page = 1
        while more
          print '.'
          response = gl_admin_request(:get, "issues?assignee_id=#{agent['gitlab']['id']}&page=#{page}")
          response.each do |issue|
            agent['gitlab']['data'][:open_issues].push(issue_object(issue))
          end
          page += 1
          more = false if response.count < 100
        end
      end

      def self.gather_open_mrs(agent)
        more = true
        page = 1
        while more
          print '.'
          response = gl_admin_request(:get, "merge_requests?author_id=#{agent['gitlab']['id']}&page=#{page}")
          response.each do |merge_request|
            agent['gitlab']['data'][:open_mrs].push(merge_object(merge_request))
          end
          page += 1
          more = false if response.count < 100
        end
      end

      def self.analyze_issue_by_creation(type, mid_date, issue)
        user = agent(issue['author']['id'])
        return if user.nil?

        if Date.parse(issue['created_at']) < mid_date
          user['gitlab']['data'][type][:previous].push(issue_object(issue))
        else
          user['gitlab']['data'][type][:current].push(issue_object(issue))
        end
      end

      def self.analyze_issue_by_closed(type, mid_date, issue)
        user = agent(issue['author']['id'])
        return if user.nil?

        if Date.parse(issue['closed_at']) < mid_date
          user['gitlab']['data'][type][:previous].push(issue_object(issue))
        else
          user['gitlab']['data'][type][:current].push(issue_object(issue))
        end
      end

      def self.analyze_handbook_mr(merge_request)
        user = agent(merge_request['author']['id'])
        return if user.nil?

        if Date.parse(merge_request['merged_at']) < this_week_start
          user['gitlab']['data'][:handbook][:previous].push(merge_object(merge_request))
        else
          user['gitlab']['data'][:handbook][:current].push(merge_object(merge_request))
        end
      end

      def self.determine_weight(merge)
        weight = merge['labels'].find { |l| /weight/ =~ l }
        (weight.nil? ? 0 : weight.split(':').last)
      end

      def self.issue_object(issue)
        { id: issue['iid'],
          url: issue['web_url'],
          title: issue['title'],
          project: issue['references']['full'].split('#').first,
          closed: issue['closed_at'],
          created: issue['created_at'] }
      end

      def self.merge_object(merge)
        { url: merge['web_url'],
          title: merge['title'],
          weight: determine_weight(merge),
          merged: merge['merged_at'],
          created: merge['created_at'] }
      end
    end
  end
end
