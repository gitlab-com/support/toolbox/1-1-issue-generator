# frozen_string_literal: true

# Generator module
module Generator
  # GitLab module
  module GitLab
    # DailyReport class
    class DailyReport < Client
      def self.commit_daily_report
        date = Date.today.to_time.in_time_zone('UTC') - 1.days
        json = File.read("data/#{date.strftime('%Y-%m-%d')}_analysis.json")
        gl_request(:post, 'projects/26161478/repository/commits', daily_report_opts(json, date))
      end

      def self.daily_report_opts(json, date)
        {
          branch: 'master',
          commit_message: 'Adding daily report',
          actions: [
            {
              action: 'create',
              file_path: "reports/#{date.strftime('%Y-%m-%d')}_analysis.json",
              content: json
            }
          ]
        }
      end
    end
  end
end
