# frozen_string_literal: true

# Generator module
module Generator
  # GitLab module
  module GitLab
    require_relative 'gitlab/daily_report'
    require_relative 'gitlab/gather'
    require_relative 'gitlab/monthly_report'
    require_relative 'gitlab/quarterly_report'
    require_relative 'gitlab/weekly_issue'
  end
end
