# frozen_string_literal: true

# The Generator module
module Generator
  require 'bundler/setup'
  require 'open-uri'
  Bundler.require(:default)
  require_relative 'generator/client'
end
