# 1-1 Issue Generator

This is the collection of files used to create 1-1 issues.

---

**NOTE**: This is a public project. Any and all data within the files of this
project could be reviewed by anyone. If you wish to keep your personal data out
of the eyes of the public, please don't commit it to the repo.

---

## User Metadata

The user metadata is stored in the
[support-team.yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml).

To utilize this, you must do the following:

* Create a project to house the 1-1 issues
* Grant the user running this `maintainer` access to the project (so it can make
  and comment on issues). In GitLab's case, this would be the
  [gl-support-bot](https://gitlab.com/gl-support-bot) user.
* Update the
  [the support-team.yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml)
  for the Support Engineer in question (see below).

### Adding the relevant info to support-team.yaml

Under each entry in the
[support-team.yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml),
you will see something like this:

```yaml
1-1-generator:
  day_of_week:
  project_id: ''
  doc-links: []
  labels: []
```

Each line has an important use for this, so knowing what to put where will help
you make the most of these scripts:

| Line | Description | Data Type |
|---|---|---|
| day_of_week | The day of the week to generate the 1-1 issue | Numeric |
| project_id | The project to put the 1-1 issues in | Numeric |
| doc-links | An array of document links to put in the 1-1 issue | Array |
| labels | An array of labels to put on the 1-1 issue | Array |

#### Day of week

This will generate your 1-1 on a day of the week of your choosing. It uses the
ruby method of numbering the days:

* 0 = Sunday
* 1 = Monday
* 2 = Tuesday
* 3 = Wednesday
* 4 = Thursday
* 5 = Friday
* 6 = Saturday

#### Doc-links

The `doc-links` part is for links to documents you want included in the 1-1
issues. You can have multiple links here in YAML array format, such as:

```yaml
    doc-links:
    - name: 'My Awesome Document'
      link: 'https://example.com/my-awesome-document'
    - name: 'My Secret Document'
      link: 'https://example.com/my-secret-document'
```

#### Labels

You can also have the script add custom labels to the generated issues. This
is done by adding the label names to the yaml block:

```yaml
    labels:
    - brie-is-cool
    - 'Jason Mood::Hungry'
```

The above format must be used to prevent problems from arising. If you need
assistance, feel free to ping @jcolyer for assistance.

## What is in the support-team.yaml file anyways?

GitLab support uses this file for a good bit of information (and across a good
number of projects). For the sake of the 1-1 generator, the most important
parts are:

```yaml
- name: NAME_OF_PERSON
  email: EMAIL_OF_PERSON
  reports_to: NAME_OF_MANAGER
  zendesk:
    main:
      id: ZENDESK_MAIN_USER_ID
    us-federal:
      id: ZENDESK_US_FEDERAL_USER_ID
  pagerduty:
    id: PAGERDUTY_USER_ID
  gitlab:
    id: GITLAB_USER_ID
    username: GITLAB_USERNAME
  1-1-generator:
    day_of_week: DAY_TO_MAKE_ISSUE
    project_id: ID_OF_PROJECT_TO_USE
    doc-links: []
    labels: []
```

## Custom Comment Templating

There might come a time where you want to customize the comments being posted
on your issues. When that comes, GREAT NEWS! This is also possible via
templating, using a yaml file in the `data/comments` folder. You would name the
file `person.yaml`, where `person` is the person's email before the `@` sign.

As an example, a template file for `jcolyer@gitlab.com` would be called
`jcolyer.yaml`. To make it easier, I recommend copying the
`data/comments/default.yaml` and modifying it from there.

The script will check for the existance of said file. If it exists, it uses it.
If it doesn't exist, it uses the default.yaml file!

## Custom agenda file

The script will also check the target project for your 1-1 issues to locate
an `agenda.md` file. It will then use this file to add items to the generated
1-1 issue. The script just takes the contents of that file and puts it on the
issue as a comment.

Once it has done so, it will then make a commit to the file to clear it out.

## How is the data determined?

The data is gathered daily and generate artifacts to be stored in a private
project. These are then used in future issue generation.

### Pagerduty

This looks into Pagerduty for our active rotations and pulls the final rendered
schedule for each one.

### Zendesk

Data collection on this has been deprecated. We recommend using Explore.

### GitLab

#### Pairings

To determine this, it runs the API request:

> projects/14978605/issues?state=closed&assignee_id=#{id}&per_page=100&order_by=updated_at&sort=desc

Where `#{id}` is the GitLab User ID.

What this is looking for is:

* Issues that are `Closed`
* Issues assigned to the GitLab User we are looking into
* Issues opened under the
  [Support Pairing](https://gitlab.com/gitlab-com/support/support-pairing)
  project

It only maps the issue to the user's pairings array if the `closed_at` date is
within the previous or current week.

#### Interviews

To determine this, it runs the API request:

> projects/1734706/issues?state=closed&assignee_id=#{id}&per_page=100"

What this is looking for is:

* Issues that are closed
* Issues assigned to the GitLab User we are looking into
* Issues opened under the
  [se-interview](https://gitlab.com/gitlab-com/support/tech-interview/se-interview)
  project

It only maps the issue to the user's interviews array if the `closed_at` date is
within the previous or current quarter.

#### Modules

To determine this, it runs the API request:

> projects/6282913/issues?state=closed&assignee_id=#{id}&per_page=100

What this is looking for is:

* Issues that are closed
* Issues assigned to the GitLab User we are looking into
* Issues opened under the
  [support-training](https://gitlab.com/gitlab-com/support/support-training)
  project

It only maps the issue to the user's modules array if:

* The `closed_at` date is within the previous or current quarter.
* The issue contains the label `module`

#### Handbook updates

To determine this, it runs the API request:

> projects/7764/merge_requests?scope=all&state=merged&per_page=100&author_id=#{agent['gitlab']['id']}

What this is looking for is:

* Merge requests that are merged
* Merge requests authored by the GitLab User we are looking into
* Merge requests opened under the
  [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com) project

It only maps the issue to the user's handbook array if the `merged_at` date is
within the previous or current month.

#### Documentation

To determine this, it runs the API request:

> projects/7764/merge_requests?scope=all&state=merged&per_page=100&author_id=#{agent['gitlab']['id']}

What this is looking for is:

* Merge requests that are merged
* Merge requests authored by the GitLab User we are looking into
* Merge requests opened under the [gitlab-org](https://gitlab.com/gitlab-org)
  group.

It only maps the issue to the user's documentation array if it contains a
documentation label and the `merged_at` date is within the previous or current
month.

#### Frontend

To determine this, it runs the API request:

> projects/7764/merge_requests?scope=all&state=merged&per_page=100&author_id=#{agent['gitlab']['id']}

What this is looking for is:

* Merge requests that are merged
* Merge requests authored by the GitLab User we are looking into
* Merge requests opened under the [gitlab-org](https://gitlab.com/gitlab-org)
  group.

It only maps the issue to the user's frontend array if it contains a
frontend-weight label and the `merged_at` date is within the previous or current
month.

#### Backend

To determine this, it runs the API request:

> projects/7764/merge_requests?scope=all&state=merged&per_page=100&author_id=#{agent['gitlab']['id']}

What this is looking for is:

* Merge requests that are merged
* Merge requests authored by the GitLab User we are looking into
* Merge requests opened under the [gitlab-org](https://gitlab.com/gitlab-org)
  group.

It only maps the issue to the user's backend array if it contains a
backend-weight label and the `merged_at` date is within the previous or current
month.

#### Design

To determine this, it runs the API request:

> projects/7764/merge_requests?scope=all&state=merged&per_page=100&author_id=#{agent['gitlab']['id']}

What this is looking for is:

* Merge requests that are merged
* Merge requests authored by the GitLab User we are looking into
* Merge requests opened under the [gitlab-org](https://gitlab.com/gitlab-org)
  group.

It only maps the issue to the user's design array if it contains a design-weight
label and the `merged_at` date is within the previous or current month.

#### Support Fixes

To determine this, it runs the API request:

> groups/9970/merge_requests?scope=all&state=merged&per_page=100&author_id=#{agent['gitlab']['id']}

What this is looking for is:

* Merge requests that are merged
* Merge requests authored by the GitLab User we are looking into
* Merge requests opened under the [gitlab-org](https://gitlab.com/gitlab-org)
  group.

It only maps the issue to the user's support-fixes array if the `merged_at` date
is within the previous or current month.

#### Gitlab-org Other

To determine this, it runs the API request:

> groups/9970/merge_requests?scope=all&state=merged&per_page=100&author_id=#{agent['gitlab']['id']}

What this is looking for is:

* Open merge request in the gitlab-org group
* Merge requests created by the GitLab User we are looking into

It only maps the issue to the user's other array if it met none of the other
categories requirements and the `merged_at` date is within the previous or
current month.

## Monthly reports

**NOTE**: This is a new, experimental feature.

The scripts enable the ability to generate a monthly report. This is done by
gathering reports from one month ago until today. From there, it analyzes them
and generates a monthly report issue.

These do not run on schedules and must be generated on-demand. To do this, you
will need to manually run a pipeline containing the following:

* `STAGE=monthly`
* `AGENT_NAME=your_name`

You will replace `your_name` with your name as it appears in the
[support-team.yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml).

From there, it will run and generate the issue for you.

## Quarterly reports

**NOTE**: This is a new, experimental feature.

The scripts enable the ability to generate a quarterly report. This is done by
gathering reports from the current quarter. From there, it analyzes them and
generates a quarterly report issue.

These do not run on schedules and must be generated on-demand. To do this, you
will need to manually run a pipeline containing the following:

* `STAGE=quarterly`
* `AGENT_NAME=your_name`

You will replace `your_name` with your name as it appears in the
[support-team.yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml).

From there, it will run and generate the issue for you.

## Could I make a schedule for my own reports?

As this runs from the ops.gitlab.net instance, you are not able to directly.
What you can do is file an issue on the project requesting Support Operations
create the schedule for you!

When adding a new schedule to the ops.gitlab.net instance, please try to avoid
running them during the hours of 0000-0300 UTC, as the daily reports and 1-1
issues are run in those time-frames. We also ask you use the naming format
`YOUR_NAME - REPORT_TYPE`. An example would be `Jason Colyer - Monthly` or
`Steve Austin - Quarterly`.

The best timing for them would be:

* Monthly report: beginning of the month
  * Example: `0 8 1 * *` - 0800 on the first of the month
* Quarterly report: near the end of the quarter
  * Example: `0 8 30 1,4,7,10 *` - 0800 on the 30th of January, April, July, and
    October
